﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefabs, enemyPrefabs, GameCanvas, SceneCamera;

    int id;

    private void Awake()
    {
        GameCanvas.SetActive(true);
    }

    public void SpawnPlayer()
    {
        if (id == 2)
        {
            float randomValue = Random.Range(-1f, 1f);

            PhotonNetwork.Instantiate(this.playerPrefabs.name, new Vector3(this.transform.position.x + 300f,
                1, this.transform.position.z + 300f), Quaternion.identity, 0);
            GameCanvas.SetActive(false);
            SceneCamera.SetActive(false);
        }
        else if (id == 1)
        {
            float randomValue = Random.Range(-1f, 1f);

            PhotonNetwork.Instantiate(this.enemyPrefabs.name, new Vector3(this.transform.position.x + 310f,
                1, this.transform.position.z + 320f), Quaternion.identity, 0);
            GameCanvas.SetActive(false);
            SceneCamera.SetActive(false);
        }
        else
        {
            Debug.Log("Silahkan Pilih Charachter");
        }
        
    }

    public void Enemy()
    {
        id = 1;
        Debug.Log(id);
    }

    public void Survivor()
    {
        id = 2;
        Debug.Log(id);
    }
}
