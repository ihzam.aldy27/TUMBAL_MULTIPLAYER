﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    private string VersioName = "0.1";
    [SerializeField]
    private GameObject usernameMenu, ConnectPanel;

    [SerializeField]
    private TMP_InputField usernameInput, CreateGameInput, JoinGameInput;

    [SerializeField]
    private GameObject StartButton;

    private void Awake()
    {
        PhotonNetwork.ConnectUsingSettings(VersioName);

    }

    private void Start()
    {
        usernameMenu.SetActive(true);
    }

    private void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("Connected");
    }

    public void ChangeUsernameInput()
    {
        if (usernameInput.text.Length >= 3)
        {
            StartButton.SetActive(true);
        }
        else
        {
            StartButton.SetActive(false);
        }
    }

    public void SetUsernameInput()
    {
        usernameMenu.SetActive(false);
        PhotonNetwork.playerName = usernameInput.text;
    }

    [System.Obsolete]
    public void CreateGame()
    {
        PhotonNetwork.CreateRoom(CreateGameInput.text, new RoomOptions() { maxPlayers = 5 }, null);
    }

    [System.Obsolete]
    public void JoinGame()
    {
        RoomOptions roomOption = new RoomOptions();
        roomOption.maxPlayers = 5;
        PhotonNetwork.JoinOrCreateRoom(JoinGameInput.text, roomOption, TypedLobby.Default);
    }

    private void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("MainGame");
    }
}
