﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonNetwork : Photon.MonoBehaviour
{
    ThirdPersonMovement tpController;

    bool firstTake = false;

    void OnEnable()
    {
        firstTake = true;    
    }

    void Awake()
    {
        tpController = GetComponent<ThirdPersonMovement>();

        if (photonView.isMine)
        {
            tpController.enabled = true;
            tpController.playerCamera.SetActive(true);
            tpController.canvasInventory.SetActive(true);
            tpController.tpCamera.SetActive(true);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            tpController.enabled = true;
            tpController.playerCamera.SetActive(false);
            tpController.canvasInventory.SetActive(false);
            tpController.isControllable = false;
            tpController.tpCamera.SetActive(false);

        }

        gameObject.name = gameObject.name + photonView.viewID;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We Own this player: send the other our data
            stream.SendNext((int)tpController._characterState);
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            //Network player, receive data
            tpController._characterState = (CharacterState)(int)stream.ReceiveNext();
            correctPlayerPos = (Vector3)stream.ReceiveNext();
            correctPlayerRot = (Quaternion)stream.ReceiveNext();

            if (firstTake)
            {
                firstTake = false;
                this.transform.position = correctPlayerPos;
                transform.rotation = correctPlayerRot;
            }
        }
    }

    private Vector3 correctPlayerPos = Vector3.one;
    private Quaternion correctPlayerRot = Quaternion.identity;

    // Update is called once per frame
    void Update()
    {
        if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        }
    }
}
