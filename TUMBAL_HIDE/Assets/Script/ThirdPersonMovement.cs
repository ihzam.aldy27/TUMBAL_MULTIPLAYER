﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using Mirror;

public enum CharacterState
{
    Idle = 0,
    Walking = 1,
    Crouch = 2,
    Walk_Crouch = 3,
    Vaulting = 4,
}

public class ThirdPersonMovement : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;
    public Transform posPlayer;
    public Vaultable vault;
    protected Rigidbody r;
    public GameObject player;
    public GameObject playerCamera;
    public GameObject tpCamera;
    public GameObject canvasInventory;
    public VaultData vaultData;
    public TextMeshPro PlayerNameText;

    public Animator anim;

    [SerializeField]
    public float speed = 6f;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public float mSpeedY = 0;
    public bool isControllable = true;
    float mGravity = -9.81f;
    public CharacterState _characterState;

    //int a = 0;
    void Start()
    {
        controller = GetComponent<CharacterController>();
        //player = GetComponent<GameObject>();
        posPlayer = GetComponent<Transform>();
        
        anim = GetComponent<Animator>();
        Debug.Log(anim);
    }

    // Update is called once per frame
    void Update()
    {
        if (isControllable)
        {
            HandledPlayerMovement();
        }

    }

    private void HandledPlayerMovement()
    {
        /*if (isLocalPlayer)
        {*/
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

            if (!controller.isGrounded)
            {
                mSpeedY += mGravity * Time.deltaTime;
            }
            else
            {
                mSpeedY = 0;
                _characterState = CharacterState.Idle;
            }

            Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
            Vector3 verticalMovement = Vector3.up * mSpeedY;
            

            if (direction.magnitude >= 0.1f)
            {
                MovementJalan(direction, verticalMovement);
                animasimovementJalan();

                if (this.player.tag == "Player")
                {
                    if (Input.GetKey(KeyCode.LeftShift))
                    {
                        AnimasiMovementCrouch();
                    }
                    else if (Input.GetKeyDown(KeyCode.Space))
                    {
                        if (vault.lompat == true)
                        {
                            Vaulting();
                        }

                    }
                    else
                    {
                        speed = 6f;
                    }
                }
            }
            else
            {
                
                AnimeIdle();

                if (this.player.tag == "Player")
                {
                    if (Input.GetKey(KeyCode.LeftShift))
                    {
                        AnimCrouch();
                    }
                    else if (Input.GetKeyDown(KeyCode.Space))
                    {
                        if (vault.lompat == true)
                        {
                            Vaulting();
                        }

                    }
                    else
                    {
                        controller.enabled = true;
                        AnimeIdle();
                    }
                }

            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

            }
            if (Input.GetKeyDown(KeyCode.LeftAlt))
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        //}
        
    }

    private void MovementJalan(Vector3 direction, Vector3 verticalMovement)
    {
        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
        float angel = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
        transform.rotation = Quaternion.Euler(0f, angel, 0f);

        Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        controller.Move(verticalMovement + (moveDir * speed) * Time.deltaTime);
    }

    private void Vaulting() 
    {
        
        anim.SetTrigger("Vault");
        _characterState = CharacterState.Vaulting;
        LeanTween.move(player, posPlayer.transform.position + transform.forward * 2f, 1f);
    }

    private void animasimovementJalan()
    {
        anim.SetFloat("Speed", 1f);
        _characterState = CharacterState.Walking;
    }

    private void AnimeIdle()
    {
        anim.SetFloat("Speed", 0);
        _characterState = CharacterState.Idle;

    }

    private void AnimCrouch()
    {
        anim.SetFloat("Speed", 0.3f);
        _characterState = CharacterState.Crouch;
    }

    private void AnimasiMovementCrouch()
    {
        speed = 3f;
        anim.SetFloat("Speed", 0.6f);
        _characterState = CharacterState.Walk_Crouch;
    }
}

/*VAULTING BACKUP
 * float movingPlayer = posPlayer.transform.position.z >= -1 ? -2 : 2;
controller.enabled = false;
anim.SetTrigger("Vault");
LeanTween.moveLocal(player, new Vector3(posPlayer.transform.position.x, 0, posPlayer.transform.position.z + movingPlayer), 1f);*/