﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    private int slot;
    private GameObject[] slotGame;

    public GameObject slotHolder;

    public Transform @throw;

    [SerializeField]
    public Transform Hand, Slot;

    PointDrop dropPoint;

    private void Start()
    {
        slot = 2;
        slotGame = new GameObject[slot];
        for(int i = 0; i < slot; i++)
        {
            slotGame[i] = slotHolder.transform.GetChild(i).gameObject;
            if (slotGame[i].GetComponent<Slot>().ItemObject == null)
                slotGame[i].GetComponent<Slot>().empty = true;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            RemoveItem(@throw);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            SelectItem(0, Slot);


        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SelectItem(1, Slot);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log(other);
        if(other.tag == "Item")
        {
            //Debug.Log("test");
            if (Input.GetKeyDown(KeyCode.E))
            {
                //Debug.Log("item");
                GameObject pickup = other.gameObject;
                ItemData data = pickup.GetComponent<ItemData>();
                if (data.pickUp)
                {
                    data.destroyItem = false;
                }
                

                AddItem(data.icon, data.ItemObject);
            }
        }
    }

    void AddItem(Sprite itemIcon, GameObject itemObj)
    {
        for (int i = 0; i < slot; i++)
        {
            if (slotGame[i].GetComponent<Slot>().empty)
            {
                itemObj.GetComponent<ItemData>().pickUp = true;

                slotGame[i].GetComponent<Slot>().ItemObject = itemObj;
                slotGame[i].GetComponent<Slot>().icon = itemIcon;

                //itemObj.transform.parent = slotGame[i].transform;
                itemObj.SetActive(false);

                /*itemObj.GetComponent<Rigidbody>().useGravity = false;
                itemObj.GetComponent<Rigidbody>().isKinematic = true;*/
                itemObj.transform.SetParent(Slot);

                //itemObj.transform.position = Hand.position + new Vector3(0, 0, 0);
                //itemObj.transform.rotation = Hand.rotation + new Vector3(0, 0, 0);


                slotGame[i].GetComponent<Slot>().SlotUpdate();
                slotGame[i].GetComponent<Slot>().empty = false;
                break;
            }
        }
    }

    
    void RemoveItem(Transform away)
    {
        for (int i = 0; i < slot; i++)
        {
            if (!slotGame[i].GetComponent<Slot>().empty)
            {
                
                GameObject game;

                game = slotGame[i].GetComponent<Slot>().ItemObject;
                
                    game.transform.parent = away.parent;
                    game.SetActive(true);
                    game.GetComponent<Rigidbody>().useGravity = true;
                    game.GetComponent<Rigidbody>().isKinematic = false;
                    game.GetComponent<ItemData>().destroyItem = false;

                    game.transform.position = away.position + new Vector3(1, 1, 1);

                    slotGame[i].GetComponent<Slot>().SlotRemove();
                    slotGame[i].GetComponent<Slot>().empty = true;
                
                
                break;
            }
        }
    }

    void SelectItem(int index, Transform itemData)
    {
        for (int i = 0; i < slot; i++)
        {
            int j = i == index ? index : 5;
            if (itemData.GetChild(j))
            {
               
                Debug.Log("Ini Slot" + itemData.name);
                Transform item = itemData.GetChild(j);
                item.gameObject.SetActive(true);
                item.GetComponent<Rigidbody>().useGravity = false;
                item.GetComponent<Rigidbody>().isKinematic = true;
                item.transform.SetParent(Hand);
                item.transform.position = Hand.position + new Vector3(0, 0, 0);
             
            }
        }
    }
}
