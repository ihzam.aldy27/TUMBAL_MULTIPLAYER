﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointDrop : MonoBehaviour
{
    [SerializeField]
    Vector3 position;
    [SerializeField]
    Transform setPoint;
    [SerializeField]
    GameObject itemDrop;

    public Inventory inven;

    void Start()
    {
        setPoint = GetComponent<Transform>();
        position = new Vector3(transform.position.x + setPoint.transform.position.x,
            transform.position.y + setPoint.transform.position.y, transform.position.z + setPoint.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item")
        {
            itemDrop = other.gameObject;
            itemDrop.transform.position = setPoint.position;
            itemDrop.transform.rotation = setPoint.rotation;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Item")
        {
            itemDrop = null;
        }
    }
}
