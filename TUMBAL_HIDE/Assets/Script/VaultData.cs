﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaultData
{
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float vaultspeed;

    public float vaultT;
}
