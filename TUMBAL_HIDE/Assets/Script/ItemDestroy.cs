﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDestroy : MonoBehaviour
{
    public ItemData itemData;
    public GameObject itemObjek;
    public Transform setPoint;
    public PointDrop dropPoint;

    void Start()
    {
        itemObjek.GetComponent<ItemData>();
    }

    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.E)) && (!itemData.pickUp))
        {
            if (itemData.pickUp)
            {
                itemData.destroyItem = false;
                Debug.Log("Item gagal dihancurkan");
                StopCoroutine(itemDestroy(itemObjek.gameObject));
            }
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.tag == "Pembakaran")
        {
            itemData.destroyItem = true;

            if (Input.GetKeyDown(KeyCode.Q))
            {
                Debug.Log("Item ini siap dihancurkan");
                StartCoroutine(itemDestroy(itemObjek));
            }
        }
    }

    IEnumerator itemDestroy(GameObject item)
    {
        Debug.Log("Waktu 5 detik lagi hancur");
         yield return new WaitForSeconds(5);
         Destroy(item);
    }
}
